#!/bin/bash
#######################
# setup.sh
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
#######################
# Depends on: antibody(aur) zsh rofi i3 pkgfile autojump mantablockscreen(aur)  
####### Variables

dir=~/dotfiles      # dotfiles directory
olddir=~/dotfiles_old   # old dotfiles backup directory
files="zshrc config/test config/picom config/i3 config/termite config/i3status aliases.conf bashrc bash_profile zprofile xbindkeysrc zsh_plugins.txt xinitrc zshrc.zni local/share/rofi moc/config moc/themes/ Xmodmaprc wallpaper"

#######

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

#change tp the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

#move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/.$file $olddir
    echo "Creating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
	done
