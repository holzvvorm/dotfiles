#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Search for unknown commands
source /usr/share/doc/pkgfile/command-not-found.bash

# lade aliase aus datei
source ~/.aliases.conf
source ~/.localaliases.conf

PS1='[\u@\h \W]\$ '


